Django==1.6.9
South==1.0.2
argparse==1.2.1
django-picklefield==0.3.2
requests==2.8.1
six==1.10.0
wsgiref==0.1.2
