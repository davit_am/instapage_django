from django.db import models
from mainapp.instapage import InstapageApiClient
from django.conf import settings

def get_pages(login, password):
	INSTAPAGE_HOST = "app.myinstapage.com"
	api = InstapageApiClient(INSTAPAGE_HOST, "local.wordpress.dev")
	user_id, plugin_hash = api.login(login, password)	
	return api.get_user_pages(user_id, plugin_hash)


# Create your models here.
class Credentials(models.Model):
	login = models.CharField(max_length=100)
	password = models.CharField(max_length=100)

class PublishPage(models.Model):
	PAGE_CHOICES = []
	credentials = settings.CREDENTIALS
	email = credentials['email']
	password = credentials['password']
	pages = get_pages(email, password)
	for page in pages:
		choice = "%s-%s"%(page['title'], page['id'])
		PAGE_CHOICES.append((choice, choice))
	slug = models.CharField(max_length=50)
	instapage = models.CharField(max_length=50, choices=PAGE_CHOICES)