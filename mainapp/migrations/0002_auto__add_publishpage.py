# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PublishPage'
        db.create_table(u'mainapp_publishpage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('slug', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('instapage', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'mainapp', ['PublishPage'])


    def backwards(self, orm):
        # Deleting model 'PublishPage'
        db.delete_table(u'mainapp_publishpage')


    models = {
        u'mainapp.credentials': {
            'Meta': {'object_name': 'Credentials'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'mainapp.publishpage': {
            'Meta': {'object_name': 'PublishPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instapage': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['mainapp']