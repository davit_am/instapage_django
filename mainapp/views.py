from django.shortcuts import render
from django.http import HttpResponse
from mainapp.instapage import InstapageApiClient
from mainapp.models import PublishPage
from django.shortcuts import get_object_or_404
from django.conf import settings
import pdb
INSTAPAGE_HOST = "app.myinstapage.com"

def insta(request, slug):
	
	credentials = settings.CREDENTIALS
	email = credentials['email']
	password = credentials['password']
	api = InstapageApiClient(INSTAPAGE_HOST, "local.wordpress.dev")
	user_id, plugin_hash = api.login(email, password)	
	#print api.get_user_pages(user_id, plugin_hash)
	page = get_object_or_404(PublishPage, slug=slug)
	insta = page.instapage.split("-")[1]
	
	instapage_content = api.get_page(insta)#'<div><h4 style="color: royalblue">Content retrieved from</h4> <a href="https://instapage.com">Instapage</a></div>'
	context = {'content': instapage_content}
	return render(request, 'mainapp/instapage.html', context)