from django.conf.urls import patterns, url

from mainapp import views

urlpatterns = patterns('',
    url(r'(?P<slug>.+?)/$', views.insta, name='insta')
)
