SECRET_KEY = "2d1484bc-aaa2-4bd4-b0b3-f4e0ddcffb35a9491e47-b954-4db5-95b9-785e3fbfb4dfa4c472eb-f860-435d-befc-a89fef91bd2a"
NEVERCACHE_KEY = "b684ed01-74f6-4d90-8162-9d21df32b7d626a180f0-b9fd-4de1-98e5-18166c6f9dec27faf333-53cd-4931-a14e-06ca6080f8de"

DATABASES = {
    "default": {
        # Ends with "postgresql_psycopg2", "mysql", "sqlite3" or "oracle".
        "ENGINE": "django.db.backends.sqlite3",
        # DB name or path to database file if using sqlite3.
        "NAME": "dev.db",
        # Not used with sqlite3.
        "USER": "",
        # Not used with sqlite3.
        "PASSWORD": "",
        # Set to empty string for localhost. Not used with sqlite3.
        "HOST": "",
        # Set to empty string for default. Not used with sqlite3.
        "PORT": "",
    }
}